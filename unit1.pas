unit Unit1;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, LazSerial, Forms, Controls, Graphics, Dialogs,
  StdCtrls, ExtCtrls, LazUTF8;

type

  { TForm1 }

  TForm1 = class(TForm)
    bSetPort: TButton;
    Button1: TButton;
    OK: TButton;
    uartSend: TEdit;
    eSerNum: TEdit;
    EditDevice: TEdit;
    Serial: TLazSerial;
    lStart: TLabel;
    Timer1: TTimer;
    uartMemo1: TMemo;
    procedure bSetPortClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure OKClick(Sender: TObject);
    procedure SerialRxData(Sender: TObject);
    procedure uartMemo1KeyPress(Sender: TObject; var Key: char);
  private

  public

  end;

var
  Form1: TForm1;

implementation

{$R *.lfm}

{ TForm1 }
const
  ConstsParity: array[TParity] of char = ('N', 'O', 'E', 'M', 'S');


//function oemtolocal(s: string): string;

//var
//  str, str1: utf8string;
//  i: integer;
//  j: byte;
//  cp1251: utf8string;
//begin
//  //ASCII CP1251 TABLE
//  cp1251 :=
//    'ЂЃ‚ѓ„…†‡€‰Љ‹ЊЌЋЏђ‘’“”•–—?™љ›њќћџ ЎўЈ¤Ґ¦§Ё©Є«¬­®Ї°±Ііґµ¶·ё№є»јЅѕїАБВГДЕЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯабвгдежзийклмнопрстуфхцчшщъыьэюя';
//  Form1.Caption := UTF8ToConsole(RightStr(cp1251,30));
//  str := '';
//  {for i := 61 to 70  do         // length(s)
//  begin
//    j := byte(cp1251[i]);
//    str1 := '';
//    if j < 128 then
//      str1 := s[i]
//    else
//      str1 := cp1251[j - 128 + 1];
//    str := str + str1;
//  end; }
//  Result := str;
//end;

procedure TForm1.bSetPortClick(Sender: TObject);
begin
  if bSetPort.Caption = 'UART' then
  begin
    Serial.ShowSetupDialog;
    EditDevice.Text := Serial.Device;
    //Serial.Device := EditDevice.Text;
    Serial.Open;
    bSetPort.Caption := 'CLOSE';
  end
  else
  begin
    Serial.Close;
    bSetPort.Caption := 'UART';
  end;

end;

procedure TForm1.FormActivate(Sender: TObject);
begin

end;

procedure TForm1.FormCreate(Sender: TObject);
begin
{$IFDEF LINUX}
  EditDevice.Text := '/dev/ttyACM0';
{$ELSE}
  EditDevice.Text := 'COM1';
{$ENDIF}
end;

procedure TForm1.OKClick(Sender: TObject);
var
  Str: string;
begin
  Str := uartSend.Text + char(13) + char(10);
  uartMemo1.Lines.Add(Str);
  Serial.WriteData(Str);
  uartSend.Text := '';
end;

procedure TForm1.SerialRxData(Sender: TObject);
var
  str: utf8string;
  cp1251: string;
begin
  uartMemo1.Lines.BeginUpdate;
  cp1251 := Serial.ReadData;

  str := WinCPToUTF8(cp1251);
  uartMemo1.Text := uartMemo1.Text + str;
  //AnsiToUTF8(Serial.ReadData);
  uartMemo1.Lines.EndUpdate;
  uartMemo1.SelStart := Length(uartMemo1.Lines.Text) - 1;
  uartMemo1.SelLength := 0;
end;

procedure TForm1.uartMemo1KeyPress(Sender: TObject; var Key: char);
var
  Str: string;
begin
  Str := '' + char(Key);
  Serial.WriteData(Str);
end;

end.
